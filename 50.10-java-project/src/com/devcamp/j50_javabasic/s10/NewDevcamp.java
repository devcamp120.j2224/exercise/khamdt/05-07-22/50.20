package com.devcamp.j50_javabasic.s10;

public class NewDevcamp {
    public static void main(String[] args) {
        System.out.println("I'm a new devcamp app");
        NewDevcamp.name("NgocNT", 30);
        NewDevcamp newApp = new NewDevcamp();
        newApp.name("Nguyen Thi Ngoc");
    }
    public void name(String name) {
        System.out.println("My name is " + name);
    }
    public static void name(String name, int age) {
        System.out.println("My name is " + name + " age is " + age);
    }
 
}
